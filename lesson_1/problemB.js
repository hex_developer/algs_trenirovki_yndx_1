const fs = require('fs')
let fileContent = fs.readFileSync("input.txt", "utf8");
console.log(fileContent);
const [a1, b1, c1] = fileContent.toString().split('\n')

let a = parseInt(a1);
let b = parseInt(b1);
let c = parseInt(c1);

if ((a + b) <= c || (a + c) <= b || (b + c) <= a || a == 0 || b == 0 || c == 0) {
    result = "NO";
} else result = "YES";

fs.writeFileSync("output.txt", result.toString())