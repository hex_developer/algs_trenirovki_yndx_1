const fs = require('fs')
let fileContent = fs.readFileSync("input.txt", "utf8");
console.log(fileContent);
const [a, str] = fileContent.toString().split('\n')

let arr = a.split(" ").map(function (item) {
    return parseInt(item, 10);
});

let troom = arr[0];
let tcond = arr[1];

if (troom != tcond) {
    if (str == "auto") {
        result = tcond;
    } else if (str == "fan") {
        result = troom;
    } else if (troom < tcond) {
        if (str == "heat") {
            result = tcond;
        } else result = troom;
    } else {
        if (str == "heat") {
            result = troom;
        } else result = tcond;
    }
} else result = troom;

fs.writeFileSync("output.txt", result.toString())