const fs = require('fs')
let fileContent = fs.readFileSync("input.txt", "utf8");
// console.log(fileContent);
const [a1, b1, c1, d1, e1] = fileContent.toString().split('\n')

let a = parseInt(a1);
let b = parseInt(b1);
let c = parseInt(c1);
let d = parseInt(d1);
let e = parseInt(e1);

let result;

if ((a <= d && b <= e) || (a <= e && b <= d) || (a <= d && c <= e) || (a <= e && c <= d) || (b <= d && c <= e) || (b <= e && c <= d))
 {
    result = "YES";
} else result = "NO";

fs.writeFileSync("output.txt", result.toString())
console.log(result);