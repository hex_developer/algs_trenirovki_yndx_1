const fs = require('fs')
let fileContent = fs.readFileSync("input.txt", "utf8");
console.log(fileContent);
const [a1, b1, c1] = fileContent.toString().split('\n')

let a = parseInt(a1);
let b = parseInt(b1);
let c = parseInt(c1);

let result = (c*c - b)/a;
if (a == 0 && b == c*c) {
    result = "MANY SOLUTIONS";
} else if (c < 0 || (result ^ 0) !== result) {
    result = "NO SOLUTION";
};

fs.writeFileSync("output.txt", result.toString())