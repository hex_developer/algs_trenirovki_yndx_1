const fs = require('fs')
let fileContent = fs.readFileSync("input.txt", "utf8");
let mas = fileContent.toString().split(' ').map(Number);

let result = 0;

for (let i = 1; i < mas.length; i++) {
    if (mas[i] > mas[i - 1] && mas[i] > mas[i + 1]) {
        result++
    }
}

fs.writeFileSync("output.txt", result.toString())