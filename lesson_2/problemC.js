const fs = require('fs')
let fileContent = fs.readFileSync("input.txt", "utf8");
const [a1, b1, c1] = fileContent.toString().split('\n')

let length = parseInt(a1);
let mas = b1.toString().split(' ').map(Number);
let num = parseInt(c1);

let result;
let count = Infinity;
// лол, прога не проходила тесты, если count = 1000, хотя по условию макс число 1000, почему???
for (let i = 0; i < length; i++) {
    if ( Math.abs(num - mas[i]) <= count ) {
        count = Math.abs(num - mas[i])
        result = mas[i]
        console.log(count, result)
    }
}

fs.writeFileSync("output.txt", result.toString())