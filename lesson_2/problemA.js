const fs = require('fs')
let fileContent = fs.readFileSync("input.txt", "utf8");
const a = [] = fileContent.toString().split(' ')

console.log(a);

let result = "YES"
let count = 1;

for (let i = 0; i < a.length - 1; i++) {
    if ((a[i + 1] - a[i]) > 0) {
        count++;
    } else {
        count = count * -1;
    }

    if (count < 0) {
        result = "NO"
        break
    }
}

fs.writeFileSync("output.txt", result.toString())