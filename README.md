# algs_trenirovki_yndx_1

## About

This is repository with my solutions on algs problems from [yandex-trenirovki-1](https://yandex.ru/yaintern/algorithm-training_1) written on NodeJS

## Contest links

[lesson 1](https://contest.yandex.ru/contest/27393/problems/)\
[lesson 2](https://contest.yandex.ru/contest/27472/problems/)\
[lesson 3](https://contest.yandex.ru/contest/27663/problems/)

## Progress

| Lesson | Task A | Task B | Task C | Task D | Task E | Task F | Task G | Task H | Task I | Task J |
|--------|--------|--------|--------|--------|--------|--------|--------|--------|--------|--------|
| 1      | ✓      | ✓      |        | ✓      |        |        |        |        | ✓      |        |
| 2      | ✓      |        | ✓      | ✓      |        |        |        |        |        |        |
| 3      |        |        |        |        |        |        |        |        |        |        |
| 4      |        |        |        |        |        |        |        |        |        |        |
| 5      |        |        |        |        |        |        |        |        |        |        |
| 6      |        |        |        |        |        |        |        |        |        |        |
| 7      |        |        |        |        |        |        |        |        |        |        |
| 8      |        |        |        |        |        |        |        |        |        |        |

